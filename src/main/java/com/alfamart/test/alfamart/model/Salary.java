package com.alfamart.test.alfamart.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "salary")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Salary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "employee_id")
    private Integer employeeId;

    @Column(name = "salary")
    private Integer salary;
}
