package com.alfamart.test.alfamart.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "role")
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId;

    @Column(name = "role_name")
    private String roleName;
}
