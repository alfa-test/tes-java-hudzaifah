package com.alfamart.test.alfamart.service;

import com.alfamart.test.alfamart.Repository.EmployeeRepository;
import com.alfamart.test.alfamart.Repository.RoleRepository;
import com.alfamart.test.alfamart.Repository.SalaryRepository;
import com.alfamart.test.alfamart.dto.EmployeeRequestDto;
import com.alfamart.test.alfamart.dto.EmployeeResponseDto;
import com.alfamart.test.alfamart.dto.UpdateRequestDto;
import com.alfamart.test.alfamart.model.Employee;
import com.alfamart.test.alfamart.model.Role;
import com.alfamart.test.alfamart.model.Salary;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepo;

    @Autowired
    private SalaryRepository salaryRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Autowired
    ModelMapper modelMapper;

    private DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    public List<EmployeeResponseDto> getAllEmployee (){

        List<EmployeeResponseDto> listResponse = new ArrayList<>();
        List<Employee> employees = employeeRepo.findAll();
        if(employees != null){
            listResponse = employees.stream().map(a->{
                Salary salary = salaryRepo.findByEmployeeId(a.getId()).orElse(null);
                EmployeeResponseDto employeeResponseDto = modelMapper.map(a, EmployeeResponseDto.class);
                employeeResponseDto.setFull_name(a.getFullName());
                employeeResponseDto.setRole_id(a.getRoleId());
                if(salary != null){
                    employeeResponseDto.setSalary(salary.getSalary());
                }
                return employeeResponseDto;
            }).collect(Collectors.toList());
            return listResponse;
        }else{
            return null;
        }
    }

    public EmployeeResponseDto getDetail(Integer id){

        Employee employee = employeeRepo.findById(id).orElse(null);

        if(employee != null){
            Salary salary = salaryRepo.findByEmployeeId(employee.getId()).orElse(null);

            EmployeeResponseDto responseDto = modelMapper.map(employee, EmployeeResponseDto.class);
            responseDto.setFull_name(employee.getFullName());
            if (salary != null){
                responseDto.setSalary(salary.getSalary());
            }

            return responseDto;
        }else return null;

    }

    public EmployeeRequestDto save (EmployeeRequestDto request) {


        Employee employee = employeeRepo.findByFullName(request.getFull_name()).orElse(null);

        if(employee == null){
            Role role = roleRepo.findById(request.getRole_id()).orElse(null);
            EmployeeRequestDto employeeResponseDto = new EmployeeRequestDto();
            String timeStr = request.getDob();
            LocalDateTime time = LocalDateTime.parse(timeStr, formatter);

            Employee setEmployee = Employee.builder()
                    .fullName(request.getFull_name())
                    .address(request.getAddress())
                    .dob(time)
                    .roleId(request.getRole_id())
                    .roleId(role.getRoleId())
                    .build();

            Salary salary = Salary.builder()
                    .salary(request.getSalary())
                    .employeeId(setEmployee.getId())
                    .build();

            employeeRepo.save(setEmployee);
            salaryRepo.save(salary);

            employeeResponseDto.setFull_name(setEmployee.getFullName());
            employeeResponseDto.setAddress(setEmployee.getAddress());
            employeeResponseDto.setDob(timeStr);
            employeeResponseDto.setSalary(salary.getSalary());
            employeeResponseDto.setRole_id(role.getRoleId());


            return employeeResponseDto;
        }else return null;
    }

    public UpdateRequestDto update (UpdateRequestDto request, Integer id){

        Employee employee = employeeRepo.findById(id).orElse(null);
        if( employee == null){
            return null;
        }else {
            String timeStr = request.getDob();
            LocalDateTime time = LocalDateTime.parse(timeStr, formatter);

            Salary salary = salaryRepo.findByEmployeeId(id).orElse(null);

            employee.setFullName(request.getFull_name());
            employee.setDob(time);
            salary.setSalary(request.getSalary());

            salaryRepo.saveAndFlush(salary);
            employeeRepo.saveAndFlush(employee);

            UpdateRequestDto response = new UpdateRequestDto();
            response.setFull_name(employee.getFullName());
            response.setDob(request.getDob());
            response.setSalary(salary.getSalary());
            return response;

        }

    }

    public boolean delete (Integer id){

        Employee employee = employeeRepo.findById(id).orElse(null);

        if(employee == null){
            return false;
        }else{
            employeeRepo.delete(employee);
            return true;
        }

    }
}
