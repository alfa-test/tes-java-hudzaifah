package com.alfamart.test.alfamart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlfamartApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlfamartApplication.class, args);
	}

}
