package com.alfamart.test.alfamart.controller;

import com.alfamart.test.alfamart.dto.EmployeeRequestDto;
import com.alfamart.test.alfamart.dto.EmployeeResponseDto;
import com.alfamart.test.alfamart.dto.Response;
import com.alfamart.test.alfamart.dto.UpdateRequestDto;
import com.alfamart.test.alfamart.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<?> getAllEmployye (){

        List<EmployeeResponseDto> response = employeeService.getAllEmployee();
        if (response == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getDetail(@PathVariable(value = "id") Integer id){

        EmployeeResponseDto responseDto = employeeService.getDetail(id);

        if (responseDto == null){
            Response response = new Response();
            response.setMessage("Gagal Menampilkan Data");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
        Response response = new Response();
        response.setService(this.getClass().getName());
        response.setMessage("berhasil menampilkan data");
        response.setData(responseDto);

        return  ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping
    public ResponseEntity<?> save (@RequestBody EmployeeRequestDto request){

        EmployeeRequestDto responseDto = employeeService.save(request);
        Response response = new Response();
        if (responseDto == null){
            response.setMessage("Gagal Membuat Data");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

        }

        response.setService(this.getClass().getName());
        response.setMessage("berhasil membuat data");
        response.setData(responseDto);

        return  ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<?> update(@RequestBody UpdateRequestDto request, @PathVariable(value = "id") Integer id){

        UpdateRequestDto responseDto = employeeService.update(request, id);
        Response response = new Response();
        if (responseDto == null){
            response.setMessage("Gagal update Data");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

        }

        response.setService(this.getClass().getName());
        response.setMessage("berhasil update data");
        response.setData(responseDto);

        return  ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id){
        boolean isDelete = employeeService.delete(id);

        Response response = new Response();
        if (isDelete == false){
            response.setMessage("Gagal menghapus Data");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }

        response.setService(this.getClass().getName());
        response.setMessage("berhasil menghapus data");
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
