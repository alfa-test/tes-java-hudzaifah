package com.alfamart.test.alfamart.controller;

import com.alfamart.test.alfamart.dto.DogResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@RestController
@RequestMapping("dog")
public class DogController {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public ResponseEntity<?> getAllDog() throws JsonProcessingException {

        String url = "https://dog.ceo/api/breeds/list/all";

        ObjectMapper objectMapper = new ObjectMapper();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(httpHeaders);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping(path = "/{id}")
    private ResponseEntity<?> getDetailDog(@PathVariable(value = "id") String id){

        String url = "https://dog.ceo/api/breed/";

        DogResponse dogResponse = new DogResponse();
        dogResponse.setBreed(restTemplate.getForObject(url + id + "/list", String.class));
        dogResponse.setImages(restTemplate.getForObject(url + id + "/images", String.class));

        return ResponseEntity.status(HttpStatus.OK).body(dogResponse);
    }
}
