package com.alfamart.test.alfamart.Repository;

import com.alfamart.test.alfamart.model.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SalaryRepository extends JpaRepository<Salary, Integer> {

    Optional<Salary> findByEmployeeId(Integer employeeId);
}
