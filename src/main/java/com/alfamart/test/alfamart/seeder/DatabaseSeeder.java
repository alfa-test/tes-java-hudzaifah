package com.alfamart.test.alfamart.seeder;

import com.alfamart.test.alfamart.Repository.EmployeeRepository;
import com.alfamart.test.alfamart.Repository.RoleRepository;
import com.alfamart.test.alfamart.model.Employee;
import com.alfamart.test.alfamart.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DatabaseSeeder {

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private EmployeeRepository employeeRepo;

    @Autowired
    JdbcTemplate jdbcTemplate;

    private DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    @EventListener
    public void seed (ContextRefreshedEvent event){
        seedRole();
        seedEmployee();
    }

    private void seedRole (){
        Role role1 = new Role();
        role1.setRoleId(1);
        role1.setRoleName("Manager");
        Role role2 = new Role();
        role2.setRoleId(2);
        role2.setRoleName("Cashier");
        Role role3 = new Role();
        role3.setRoleId(3);
        role3.setRoleName("Staff");
        Role role4 = new Role();
        role4.setRoleId(4);
        role4.setRoleName("Receptionist");
        Role role5 = new Role();
        role5.setRoleId(5);
        role5.setRoleName("Security");
        roleRepo.save(role1);
        roleRepo.save(role2);
        roleRepo.save(role3);
        roleRepo.save(role4);
        roleRepo.save(role5);
    }

    private void seedEmployee(){

        String timeStr = "1971-06-28T00:00:00Z";
        Employee employee = new Employee();
        employee.setFullName("Adi Purnama");
        employee.setAddress("Depok");
        employee.setDob(LocalDateTime.parse(timeStr, formatter));
        employee.setId(1);
        employeeRepo.save(employee);
    }
}
