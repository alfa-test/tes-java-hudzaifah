package com.alfamart.test.alfamart.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter @Setter
public class EmployeeRequestDto {

    @NotBlank(message = "Full Name Tidak Boleh Kosong")
    private String full_name;

    @NotBlank(message = "Address Tidak Boleh Kosong")
    private String address;

    private String dob;

    @NotBlank(message = "Salary Tidak Boleh Kosong")
    private Integer salary;

    @NotBlank(message = "Role Id tidak boleh Kosong")
    private Integer role_id;

}
