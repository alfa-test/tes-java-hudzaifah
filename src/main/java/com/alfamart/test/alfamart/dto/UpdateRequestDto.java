package com.alfamart.test.alfamart.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter @Setter
public class UpdateRequestDto {

    @NotBlank(message = "Full Name Tidak Boleh Kosong")
    private String full_name;

    @NotBlank(message = "Salary Tidak Boleh Kosong")
    private Integer salary;

    private String dob;
}
