package com.alfamart.test.alfamart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DogResponse {

    private String breed;
    private String images;
}
