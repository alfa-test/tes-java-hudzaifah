package com.alfamart.test.alfamart.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter
public class EmployeeResponseDto {

    private Integer id;

    private String full_name;

    private String address;

    private LocalDateTime dob;

    private Integer role_id;

    private Integer salary;
}
